using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicTransition : MonoBehaviour
{
    [SerializeField]
    private float musicValue;

    private void Awake()
    {
        if (GameObject.FindGameObjectWithTag("Music") == null)
        {
            return;
        }

        GameObject.FindGameObjectWithTag("Music").GetComponent<MusicManager>().PlayMusic("Transition", musicValue);
    }
}
