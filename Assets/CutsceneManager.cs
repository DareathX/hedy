using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using Cinemachine;

public class CutsceneManager : MonoBehaviour
{
    private PlayableDirector timeline;
    public bool isPlaying = true;

    [SerializeField]
    private CinemachineVirtualCamera camera;

    private void Start()
    {
        timeline = GetComponent<PlayableDirector>();
    }

    public void PlayCutscene()
    {
        if (isPlaying == true)
        {
            StartCoroutine(IsPlaying());
            isPlaying = false;
        }
    }

    private IEnumerator IsPlaying()
    {
        camera.Priority = 22;
        timeline.Play();

        do
        {
            yield return null;
        } while (timeline.state == PlayState.Playing);


        camera.Priority = 10;
    }
}
