using UnityEngine;

public class Movement : MonoBehaviour
{
    private Vector3 direction = Vector3.zero;
    [SerializeField]
    private CharacterController charController;
    [SerializeField]
    private float speed = 6;
    private PlayerCollision collision;

    private float turnSmoothTime = 0.1f;
    private float turnSmoothVelocity;

    private bool gridMovement;
    private bool gridMoving;
    private Vector3 nextPos;
    private Vector3 lookAt;
    Vector3 increment;
    private ThreeDGrid grid;

    private Animator anim;

    private void Start()
    {
        // Retrieves character
        charController = GetComponent<CharacterController>();
        collision = GetComponent<PlayerCollision>();
        anim = GetComponent<Animator>();
        gridMovement = false;
        gridMoving = false;
        increment = Vector3.zero;

        GetGrid();
    }

    private void Update()
    {
        if ((gridMovement || gridMoving) && grid != null)
            GridMovement();
        else
            NormalMovement();
    }
    private void OnControllerColliderHit(ControllerColliderHit collision)
    {
        // When the collided object is pushable, add force in the same direction as the player is moving to.
        if (collision.collider.gameObject.tag == "Pushable")
        {
            Rigidbody body = collision.collider.attachedRigidbody;

            if (body == null || body.isKinematic) return;

            Vector3 pushDir = new Vector3(collision.moveDirection.x, 0, collision.moveDirection.z);

            body.velocity = pushDir * 4;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "GridEntrance")
        {
            Vector3 dir = other.transform.position - transform.position;
            dir.y = 0;
            if (Mathf.Abs(dir.x) > Mathf.Abs(dir.z)) dir.z = 0;
            else dir.x = 0;
            dir.Normalize();
            nextPos = other.transform.position + dir;
            gridMoving = true;
            if (gridMovement)
            {
                grid.RemovePlayer();
                gridMovement = false;
            }
            else
            {
                grid.PlacePlayer(nextPos - grid.transform.parent.transform.position);
                gridMovement = true;
            }
            transform.position = Vector3.MoveTowards(transform.position, nextPos, Time.deltaTime * speed);
        }
    }

    private void NormalMovement()
    {
        // Implements movement
        direction.x = Input.GetAxis("Horizontal") * speed;
        direction.z = Input.GetAxis("Vertical") * speed;
        PlayerGravity();

        transform.TransformDirection(direction);
        direction = Vector3.ClampMagnitude(direction, 2f);
        charController.Move(direction * speed * Time.deltaTime);
        if (direction.x != 0f || direction.z != 0f)
        {
            // Lets the player smoothly turn around
            // Needs to be in this if statement, otherwise it'll reset the rotation.y to 0
            // since there is no player input
            IsRotating();

            Walk();
        }
        else
        {
            Idle();
        }
    }

    private void GridMovement()
    {
        if (collision.IsPushing()) return;
        transform.TransformDirection(direction);
        if (!gridMoving)
        {
            gridMoving = true;
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
            {
                increment = new Vector3(0, 0, 1);
                if (grid.CanPushHere((transform.position + increment) - grid.transform.parent.transform.position))
                {
                    nextPos = transform.position + increment;
                    grid.MoveHere(transform.position - grid.transform.parent.transform.position, nextPos - grid.transform.parent.transform.position);
                }
                direction.z = 1;
                direction.x = 0;
            }
            else if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                increment = new Vector3(-1, 0, 0);
                if (grid.CanPushHere((transform.position + increment) - grid.transform.parent.transform.position))
                {
                    nextPos = transform.position + increment;
                    grid.MoveHere(transform.position - grid.transform.parent.transform.position, nextPos - grid.transform.parent.transform.position);
                }
                direction.x = -1;
                direction.z = 0;
            }
            else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
            {
                increment = new Vector3(0, 0, -1);
                if (grid.CanPushHere((transform.position + increment) - grid.transform.parent.transform.position))
                {
                    nextPos = transform.position + increment;
                    grid.MoveHere(transform.position - grid.transform.parent.transform.position, nextPos - grid.transform.parent.transform.position);
                }
                direction.z = -1;
                direction.x = 0;
            }
            else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                increment = new Vector3(1, 0, 0);
                if (grid.CanPushHere((transform.position + increment) - grid.transform.parent.transform.position))
                {
                    nextPos = transform.position + increment;
                    grid.MoveHere(transform.position - grid.transform.parent.transform.position, nextPos - grid.transform.parent.transform.position);
                }
                direction.x = 1;
                direction.z = 0;
            }
            else
            {
                gridMoving = false;
                Idle();
            }
        }
        else
        {
            if (!gridMovement || nextPos != transform.position)
            {
                transform.position = Vector3.MoveTowards(transform.position, nextPos, Time.deltaTime * speed);
                Walk();
            }
            else
            {
                nextPos = transform.position;
            }
            if (nextPos == transform.position)
            {
                gridMoving = false;
                lookAt = transform.position + increment;
            }
        }
        if (direction != Vector3.zero)
        {
            if (!IsRotating())
                direction = Vector3.zero;
        }
    }

    private void Idle()
    {
        anim.SetBool("IsPushing", false);
        anim.SetBool("IsWalking", false);
    }

    private void Walk()
    {
        anim.SetBool("IsPushing", false);
        anim.SetBool("IsWalking", true);
    }

    public void Push()
    {
        anim.SetBool("IsPushing", true);
        anim.SetBool("IsWalking", false);
    }

    private bool IsRotating()
    {
        float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
        float angle = Mathf.SmoothDampAngle(
            transform.eulerAngles.y,
            targetAngle,
            ref turnSmoothVelocity,
            turnSmoothTime
        );
        transform.rotation = Quaternion.Euler(0f, angle, 0f);
        if (angle == targetAngle)
            return false;
        return true;
    }

    private void PlayerGravity()
    {
        if (!charController.isGrounded)
        {
            direction = Physics.gravity;
        }
    }

    public Vector3 GetNextGridPosition()
    {
        return lookAt;
    }

    public bool IsInGrid()
    {
        return gridMovement && !gridMoving;
    }

    private void GetGrid()
    {
        GameObject go = GameObject.Find("Grid");
        if (go != null)
        {
            grid = go.GetComponent<ThreeDGrid>();
        }
    }
}