using UnityEngine;

public class HubController : MonoBehaviour
{
    private GameObject objCheck;
    private GameObject[] npcs;
    /// <summary>
    /// Sets NPC of the next active when the current level is completed. The NPCs of previous levels stay active.
    /// </summary>
    void Start()
    {
        npcs = GameObject.FindGameObjectsWithTag("NPC");
        objCheck = GameObject.Find("CheckLevel");

        int currentLevel = objCheck.GetComponent<LevelCheck>().level;

        for (int i = 0; i < npcs.Length; i++)
        {
            if (npcs[i].GetComponent<NpcController>().level <= currentLevel || npcs[i].GetComponent<NpcController>().level == 1) // on first start, only npc of the first level must be active
            {
                npcs[i].SetActive(true);
            }
            else npcs[i].SetActive(false);
        }
    }
}
