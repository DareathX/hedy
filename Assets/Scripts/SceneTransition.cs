﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    public class SceneTransition : MonoBehaviour
    {
        //-------------Call like below--------------------
        //var sceneTrans = GameObject.Find("SceneTransition");
        //sceneTrans.GetComponent<SceneTransition>().LoadNextScene(nameOfScene);
        public Animator animator;
        public void LoadNextScene(string scenename)
        {
            StartCoroutine(LoadSceneWithFade(scenename));
        }

        IEnumerator LoadSceneWithFade(string scenename)
        {
            animator.SetTrigger("Start");

            yield return new WaitForSeconds(1f);

            SceneManager.LoadScene(scenename);
        }
    }
}