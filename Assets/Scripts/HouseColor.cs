using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class HouseColor : MonoBehaviour
{
    private Material body;
    private Material top;
    private GameObject grid;

    [SerializeField] private CutsceneManager cutsceneManager;

    void Start()
    {
        body = GetComponent<Renderer>().materials.Where(x => x.name.Equals("House Body (Instance)")).FirstOrDefault();
        top = GetComponent<Renderer>().materials.Where(x => x.name.Equals("House Top (Instance)")).FirstOrDefault();
        grid = GameObject.Find("Grid");
    }

    void Update()
    {
        GridTile block = grid.GetComponent<ThreeDGrid>().GetGrid()[7, 5];
        if (block.type == TileType.PUSHABLE)
        {
            PushableObject pushableObject = block.gameObj.GetComponent<PushableObject>();
            SetColor(pushableObject.codeString);

            cutsceneManager.PlayCutscene();
        }
        else
        {
            cutsceneManager.isPlaying = true;
        }
    }

    private void SetColor(string color)
    {
        print(color);
        switch (color)
        {
            case "red":
                body.color = Color.red;
                top.color = Color.red;
                break;
            case "blue":
                body.color = Color.blue;
                top.color = Color.blue;
                break;
            case "yellow":
                body.color = Color.yellow;
                top.color = Color.yellow;
                break;
            case "green":
                body.color = Color.green;
                top.color = Color.green;
                break;
        }
    }
}
