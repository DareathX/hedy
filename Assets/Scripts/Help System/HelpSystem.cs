using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HelpSystem : MonoBehaviour
{
    private bool timerOn;
    private float timeSpent;
    private Dictionary<HelpItem, bool> helpItems;

    private void Start()
    {
        timerOn = true;
        helpItems = new Dictionary<HelpItem, bool>();
        foreach (HelpItem helpItem in transform.GetComponents<HelpItem>())
        {
            helpItems.Add(helpItem, false);
        }
    }

    private void Update()
    {
        Timer();
        foreach (HelpItem helpItem in helpItems.Keys.ToList())
        {
            helpItems[helpItem] = helpItem.CheckHelpNeeded(timeSpent);
        }
        CheckHelp();
    }

    private void CheckHelp()
    {
        // Update this to work with the dialog system
        string helpText = "";
        foreach (KeyValuePair<HelpItem, bool> helpItem in helpItems)
        {
            if (helpItem.Value)
            {
                helpText = helpItem.Key.GetHelpMsg();
                print("Hint: " + helpText);
            }
        }
    }

    private void Timer()
    {
        if (timerOn) timeSpent += Time.deltaTime;
    }

    public void PauseTimer(bool on)
    {
        timerOn = on;
    }
}