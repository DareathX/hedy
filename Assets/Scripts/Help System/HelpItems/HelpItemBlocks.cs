using System.Collections.Generic;
using UnityEngine;

public class HelpItemBlocks : HelpItem
{
    [SerializeField]
    private List<GameObject> blocks;
    private List<Vector3> blocksPos;
    private float lastBlockMovement;

    private void Start()
    {
        blocksPos = new List<Vector3>(blocks.Count);
        foreach (GameObject block in blocks)
        {
            blocksPos.Add(block.transform.position);
        }
    }

    public override bool CheckHelpNeeded(float timeSpent)
    {
        bool blockMoved = false;
        for (int i = 0; i < blocksPos.Count; i++)
        {
            if (blocksPos[i] != blocks[i].transform.position)
            {
                blockMoved = true;
            }
        }
        if (!blockMoved)
        {
            if (timeSpent - lastBlockMovement > time)
            {

                lastBlockMovement = timeSpent;
                return true;
            }
        }
        else
        {
            for (int i = 0; i < blocksPos.Count; i++)
            {
                blocksPos[i] = blocks[i].transform.position;
            }
            lastBlockMovement = timeSpent;
        }
        return false;
    }

    public override void SetHelpMsg()
    {
        helpMsg = "Try to move the block(s): ";
        foreach (GameObject block in blocks)
        {
            helpMsg += "[" + block.name + "] ";
        }
    }
}
