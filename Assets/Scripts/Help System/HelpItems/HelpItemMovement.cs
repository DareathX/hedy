using UnityEngine;

public class HelpItemMovement : HelpItem
{
    private GameObject player;
    private Vector3 playerPos;
    private float lastPlayerMovement;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    public override bool CheckHelpNeeded(float timeSpent)
    {
        if (playerPos == player.transform.position)
        {
            if (timeSpent - lastPlayerMovement > time)
            {
                lastPlayerMovement = timeSpent;
                return true;
            }
        }
        else
        {
            lastPlayerMovement = timeSpent;
            playerPos = player.transform.position;
            return false;
        }
        return false;
    }

    public override void SetHelpMsg()
    {
        helpMsg = "You can move with the arrow keys or the WASD keys\n";
    }
}
