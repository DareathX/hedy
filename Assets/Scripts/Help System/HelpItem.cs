using UnityEngine;

public abstract class HelpItem : MonoBehaviour
{
    [SerializeField]
    protected float time;
    protected string helpMsg;

    public abstract bool CheckHelpNeeded(float timeSpent);
    public abstract void SetHelpMsg();
    public string GetHelpMsg()
    {
        SetHelpMsg();
        return helpMsg;
    }
}
