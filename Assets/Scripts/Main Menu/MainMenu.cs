using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void PlayButton()
    {
        //When the play button is pressed you will go to the planet hub
        SceneManager.LoadScene(sceneName: "SpaceshipHub");
    }
    public void ExitButton()
    {
        //When the exit button is pressed you exit the game
        Application.Quit();
    }
}
