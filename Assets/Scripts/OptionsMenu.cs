using UnityEngine;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour
{
    public GameObject musicOn;
    public GameObject musicOff;
    public GameObject volumeOn;
    public GameObject volumeOff;
    public Slider musicSlider;
    private static float musicValue = 1f;
    private void Start()
    {        
        musicSlider.value = musicValue;
    }
    public void SetMusic(float music)
    {
        musicValue = music;
        MusicManager.introMusic.setVolume(music);
        if (music == 0)
        {
            musicOff.SetActive(true);
            musicOn.SetActive(false);
        }
        else
        {
            musicOff.SetActive(false);
            musicOn.SetActive(true);
        }
    }
    public void SetVolume(float volume)
    {
        if (volume == 0)
        {
            volumeOff.SetActive(true);
            volumeOn.SetActive(false);
        }
        else
        {
            volumeOff.SetActive(false);
            volumeOn.SetActive(true);
        }
    }
}
