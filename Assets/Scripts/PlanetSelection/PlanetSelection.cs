using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlanetSelection : MonoBehaviour
{
    private GameObject alienship;
    private Ray ray;
    private RaycastHit hit;
    private PlayableDirector timeline;
    private bool isFlickering = false;
    private PauzeMenu pause;

    public GameObject cutscenePlanet;

    public Transform planetPanel;
    private Animator animator;

    private void Awake()
	{
        alienship = GameObject.FindGameObjectWithTag("Alienship");
        animator = planetPanel.Find("PlanetInfo").gameObject.GetComponent<Animator>();
        planetPanel.gameObject.SetActive(false);
        pause = GameObject.Find("MenuCanvas").GetComponent<PauzeMenu>();
    }

    void Update()
	{
        if (Input.GetKeyDown(KeyCode.Q) && planetPanel.gameObject.activeSelf) 
        {
            planetPanel.gameObject.SetActive(false);
		}
        if (timeline != null)
		{
            if (isFlickering == false)
			{
                StartCoroutine(BeaconAlienShip());
			}
		}
    }

    IEnumerator BeaconAlienShip()
	{
        List<Material> materials = alienship.GetComponent<Renderer>().materials.ToList();

        Material antenne = materials.Where(m => m.name.Equals("antenne (Instance)")).FirstOrDefault();
        Material[] bolletjes = materials.Where(m => m.name.Equals("bolletjes_blauw_beneden (Instance)")).ToArray();

        yield return new WaitForSeconds((float) 0.5);

        isFlickering = true;
        antenne.EnableKeyword("_EMISSION");
        foreach (Material bolletje in bolletjes)
            bolletje.EnableKeyword("_EMISSION");

        yield return new WaitForSeconds((float) 0.5);

        isFlickering = false;
        antenne.DisableKeyword("_EMISSION");
        foreach (Material bolletje in bolletjes)
            bolletje.DisableKeyword("_EMISSION");
    }

    void OnMouseDown()
    {
        if (!pause.pauzed) {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (!planetPanel.gameObject.activeSelf && hit.collider.tag.Equals("Planet"))
                {
                    if (GameObject.Find("DialogueCanvas") != null)
                    {
                        GameObject.Find("DialogueCanvas").SetActive(false);
                    }
                    ShowPlanetInfo(hit.collider.name);

                    planetPanel.gameObject.SetActive(true);

                    bool isOpen = animator.GetBool("open");

                    animator.SetBool("open", !isOpen);
                }
            }
        }
    }

    void ShowPlanetInfo(string name)
	{
        IPlanet planet = null;
        Transform panel = planetPanel.Find("PlanetInfo");

        switch (name) {
            case "Tuto Planet":
                planet = new TutoPlanet();
            break;
            case "Music Planet":
                planet = new MusicPlanet();
            break;
            case "Water Planet":
                planet = new WaterPlanet();
            break;
            case "Food Planet":
                planet = new FoodPlanet();
            break;
        }

        planet.SetStory(panel);
        planet.SetDifficulty(panel);
        planet.SetMissions(panel);
        SetButton(cutscenePlanet, planet.GetMissionHubScene());
    }

    public void SetButton(GameObject cutscene, string missionHubScene)
	{
        Button btn = planetPanel.Find("PlanetInfo").Find("SelectButton").GetComponent<Button>();
        btn.onClick.AddListener(delegate { StartCoroutine(GoToPlanet(cutscene, missionHubScene)); } );
    }

    IEnumerator GoToPlanet(GameObject cutscene, string missionHubScene)
    {
        planetPanel.gameObject.SetActive(false);
        timeline = cutscene.GetComponent<PlayableDirector>();

        timeline.Play();

        yield return new WaitForSeconds((float) timeline.duration);
        SceneManager.LoadScene(missionHubScene);
    }
}
