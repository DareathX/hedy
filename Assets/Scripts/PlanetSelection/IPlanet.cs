using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlanet
{
    void SetStory(Transform panel);
    void SetMissions(Transform panel);
    void SetDifficulty(Transform panel);
    string GetMissionHubScene();
}
