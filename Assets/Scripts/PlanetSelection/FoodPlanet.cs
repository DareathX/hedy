using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FoodPlanet : IPlanet
{
	private string missionHubScene = "TutorialPlanetHub";

	public void SetDifficulty(Transform panel)
	{
		panel.Find("Difficulty").GetComponent<TextMeshProUGUI>().text = "Difficulty level: easy";
	}

	public void SetMissions(Transform panel)
	{
		panel.Find("Missions").GetComponent<TextMeshProUGUI>().text = "Missions to complete: 5";
	}

	public void SetStory(Transform panel)
	{
		string story = "Food Planet disasters storytelling";

		panel.Find("Story").GetComponent<TextMeshProUGUI>().text = story;
	}

	public string GetMissionHubScene()
	{
		return missionHubScene;
	}
}
