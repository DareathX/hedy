using UnityEngine;
using UnityEngine.SceneManagement;

public class TeleporterController : MonoBehaviour
{
    [SerializeField] private string nameOfScene; // name of next scene
    [SerializeField] private bool isHub = false; // Set true if the NPC is in the hub of a planet. When true there is no need to check whether the lines of code are completed.
    [SerializeField] public int level; // Represents the level number. Set 0 if isHub.

    private GameObject grid;
    private GameObject objCheck;
    private BlockSequenceManager blockSequenceManager;

    private void Awake()
    {
        objCheck = GameObject.Find("CheckLevel");
        if (!isHub)
        {
            grid = GameObject.Find("Grid");
            blockSequenceManager = grid.GetComponent<BlockSequenceManager>();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isHub) // No need to check anything, just load next scene
        {
            SceneManager.LoadScene(nameOfScene);
        }
        else if (blockSequenceManager.IsLineCompleted()) // In a level, only load next scene if all lines of code are completed.
        {
            if (objCheck == null)
            {
                GameObject levelcheck = new GameObject();
                levelcheck.name = "CheckLevel";
                levelcheck.AddComponent<LevelCheck>();
                levelcheck.GetComponent<LevelCheck>().level = level + 1;
                DontDestroyOnLoad(levelcheck);
                SceneManager.LoadScene("TutorialPlanetHub");

            }
            else
            {
                if (objCheck.GetComponent<LevelCheck>().level == level) // CurrentLevel is completed
                {
                    objCheck.GetComponent<LevelCheck>().level += 1; // Sets sequence of next level
                }
            }
            SceneManager.LoadScene(nameOfScene);
        }
    }

}
