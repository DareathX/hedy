using TMPro;
using UnityEngine;

public class NpcController : MonoBehaviour
{
    [SerializeField] private string beforeText;
    [SerializeField] private string afterText;
    [SerializeField] private string wrongText;
    [SerializeField] private GameObject teleporter;
    [SerializeField] private GameObject canvas;


    [SerializeField] private bool isHub = false; // Set true if the NPC is in the hub of a planet. When true there is no need to check whether the lines of code are completed.
    [SerializeField] public int level; // Represents the level number. Set 0 if isHub.

    private TextMeshPro missionText;
    private GameObject textObject;
    private GameObject grid;
    private BlockSequenceManager blockSequenceManager;

    void Start()
    {
        if (canvas != null)
        {
            canvas.SetActive(false);
        }
        textObject = gameObject.transform.GetChild(0).gameObject;
        missionText = textObject.GetComponent<TextMeshPro>();
        teleporter.SetActive(false);
        if (!isHub)
        {
            grid = GameObject.Find("Grid");
            blockSequenceManager = grid.GetComponent<BlockSequenceManager>();
        }
    }

    void Update()
    {
        CheckCloseToTag(4);
        if (!isHub)
        {
            if (blockSequenceManager.IsLineCompleted())
            {
                teleporter.SetActive(true);
                canvas.SetActive(true);
            }
            else
            {
                teleporter.SetActive(false);
                canvas.SetActive(false);
            }
        }
        if (isHub && gameObject.activeSelf)
        {
            teleporter.SetActive(true);
        }
    }


    // checks distance between player and NPC and also add exclamation mark
    private void CheckCloseToTag(float minimumDistance)
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (Vector3.Distance(transform.position, player.transform.position) <= minimumDistance)
        {
            if (!isHub)
            {
                if (blockSequenceManager.IsLineCompleted())
                {
                    missionText.text = afterText;
                    return;
                }
                else missionText.text = wrongText;
            }
            else missionText.text = afterText;
        }
        else missionText.text = beforeText;
    }
}
