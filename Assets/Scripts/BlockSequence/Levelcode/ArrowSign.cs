using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ArrowSign : MonoBehaviour
{
    private GameObject objtextleft;
    private GameObject objtextright;
    private TextMeshPro textleft;
    private TextMeshPro textright;
    private GameObject grid;
    private ThreeDGrid threeDGrid;
    private bool firstRepeatToggle;
    private bool secondRepeatToggle;

    [SerializeField] private CutsceneManager cutsceneManager;

    // Start is called before the first frame update
    void Start()
    {
        grid = GameObject.Find("Grid");
        objtextleft = GameObject.Find("Left");
        objtextright = GameObject.Find("Right");
        textleft = objtextleft.GetComponent<TextMeshPro>();
        textright = objtextright.GetComponent<TextMeshPro>();
        threeDGrid = grid.GetComponent<ThreeDGrid>();
    }

    // Update is called once per frame
    void Update()
    {
        GridTile blockOnTheLeft = threeDGrid.GetGrid()[4, 5];
        GridTile blockOnTheRight = threeDGrid.GetGrid()[4, 3];
        if (blockOnTheLeft.type == TileType.PUSHABLE)
        {
            PushableObject pushableObject = blockOnTheLeft.gameObj.GetComponent<PushableObject>();
            textleft.SetText(pushableObject.codeString);

            if (firstRepeatToggle)
            {
                cutsceneManager.PlayCutscene();
                firstRepeatToggle = false;
            }
        }
        else
        {
            textleft.SetText("");
            firstRepeatToggle = true;
            cutsceneManager.isPlaying = true;
        }
        if (blockOnTheRight.type == TileType.PUSHABLE)
        {
            PushableObject pushableObject = blockOnTheRight.gameObj.GetComponent<PushableObject>();
            textright.SetText(pushableObject.codeString);

            if (secondRepeatToggle)
            {
                cutsceneManager.PlayCutscene();
                secondRepeatToggle = false;
            }
        }
        else
        {
            textright.SetText("");
            secondRepeatToggle = true;
            cutsceneManager.isPlaying = true;
        }
    }
}
