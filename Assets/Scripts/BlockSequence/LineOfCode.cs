public class LineOfCode
{
    // Represents the line -> for example if 1, all blocks with codeLineNumber one belong to this line
    public int lineNumber;
    // Represents total count of blocks which this line consists of
    public int totalSequence;
    // Is true when all blocks forms the right sequence for this line
    public bool isCompleted;
    // Represents sum of blocks which are correct formed for this line
    public int currentSequence;
    // String representation of the line of code -> example "Print Hello World!"
    public string codeLine;
}
