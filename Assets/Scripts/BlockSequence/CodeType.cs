public enum CodeType
{
    Statement,
    Condition,
    Variable,
    Value
}