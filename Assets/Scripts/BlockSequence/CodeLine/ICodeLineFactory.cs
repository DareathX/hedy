﻿using System.Collections.Generic;

namespace Assets.Scripts.BlockSequence
{
    /// <summary>
    /// Interface for LineOfCodes for level scenes
    /// </summary>
    public interface ICodeLineFactory
    {
        List<LineOfCode> GetSceneCodeLines();
    }
}
