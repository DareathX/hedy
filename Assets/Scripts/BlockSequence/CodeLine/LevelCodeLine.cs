using Assets.Scripts.BlockSequence;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelCodeLine : MonoBehaviour, ICodeLineFactory
{

    private GameObject grid;
    public GameObject text;

    [SerializeField] private string codeline;
    [SerializeField] private int currentsequence;
    [SerializeField] private int totalsequence;
    [SerializeField] private string codeline2 = "";
    [SerializeField] private int currentsequence2;
    [SerializeField] private int totalsequence2;
    [SerializeField] private string codeline3 = "";
    [SerializeField] private int currentsequence3;
    [SerializeField] private int totalsequence3;

    [SerializeField] private CutsceneManager cutsceneManager;

    List<LineOfCode> lines = new List<LineOfCode>();
    private BlockSequenceManager blockSequenceManager;

    public void Start()
    {
        grid = GameObject.Find("Grid");
        blockSequenceManager = grid.GetComponent<BlockSequenceManager>();
    }

    public void Update()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        string sceneName = currentScene.name;
        if (sceneName.Equals("TutorialLevel1"))
        {
            if (blockSequenceManager.IsLineCompleted())
            {
                cutsceneManager.PlayCutscene();
                text.SetActive(true);
            }
            else
            {
                text.SetActive(false);
                cutsceneManager.isPlaying = true;
            }
        }
        if (sceneName.Equals("TutorialLevel3"))
        {
            if (blockSequenceManager.IsLineCompleted())
            {
                text.gameObject.GetComponent<TextMeshPro>().text = "Logged in";
                cutsceneManager.PlayCutscene();
            }
            else
            {
                text.gameObject.GetComponent<TextMeshPro>().text = "Enter password";
                cutsceneManager.isPlaying = true;
            }
        }
    }

    /// <summary>
    /// Get the LineOfCodes for the Scene. Each level scene needs a class like this which implements 
    /// both MonoBehaviour and ICodeLineFactory in order to work properly.
    /// </summary>
    /// <returns></returns>
    public List<LineOfCode> GetSceneCodeLines()
    {
        LineOfCode line1 = new LineOfCode()
        {
            codeLine = codeline,
            currentSequence = currentsequence,
            isCompleted = false,
            lineNumber = 1,
            totalSequence = totalsequence
        };
        lines.Add(line1);
        if (codeline2.Length > 0)
        {
            LineOfCode line2 = new LineOfCode()
            {
                codeLine = codeline2,
                currentSequence = currentsequence2,
                isCompleted = false,
                lineNumber = 2,
                totalSequence = totalsequence2
            };
            lines.Add(line2);
        }
        if (codeline3.Length > 0)
        {
            LineOfCode line3 = new LineOfCode()
            {
                codeLine = codeline3,
                currentSequence = currentsequence3,
                isCompleted = false,
                lineNumber = 3,
                totalSequence = totalsequence3
            };
            lines.Add(line3);
        }

        return lines;
    }
}


