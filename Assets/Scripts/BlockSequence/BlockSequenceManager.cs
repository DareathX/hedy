using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BlockSequenceManager : MonoBehaviour
{
    public List<LineOfCode> lines = new List<LineOfCode>();
    /// <summary>
    /// Returns true if line is completed, else false.
    /// </summary>
    /// <param name="lineNumber"></param>
    /// <returns></returns>
    public bool IsLineCompleted()
    {
        bool check = true;
        foreach (LineOfCode line in lines)
        {
            if (!line.isCompleted)
            {
                check = false;
            }
        }
        return check;
    }

    /// <summary>
    /// Adds a block to the sequence of the LineOfCode the block belongs to.
    /// </summary>
    /// <param name="lineNumber"></param>
    /// <param name="blockSequence"></param>
    public void AddSequenceToLine(int lineNumber, int blockSequence)
    {
        // if blockSequence is the next in the line sequence, currentSequence of the line is increased by 1
        if (lines.First(x => x.lineNumber == lineNumber).currentSequence == blockSequence - 1)
        {
            lines.First(x => x.lineNumber == lineNumber).currentSequence++;
        }
        // if current seq of the line equals total sequence, the line of code is completed.
        if (lines.First(x => x.lineNumber == lineNumber).currentSequence == lines.First(x => x.lineNumber == lineNumber).totalSequence)
        {
            lines.First(x => x.lineNumber == lineNumber).isCompleted = true;
        }
    }

    /// <summary>
    /// Removes a block from the sequence of the LineOfCode the block belongs to.
    /// </summary>
    /// <param name="lineNumber"></param>
    /// <param name="blockSequence"></param>
    public void RemoveSequenceFromLine(int lineNumber, int blockSequence)
    {
        IEnumerable<LineOfCode> temp = lines.Where(x => x.lineNumber == lineNumber);

        if (temp.Any())
        {
            LineOfCode line = temp.First();

            // current sequence of the line is decreased by 1 when the block moving was the last in the current seq.
            if (line.currentSequence == blockSequence)
            {
                lines.First(x => x.lineNumber == lineNumber).currentSequence--;
                lines.First(x => x.lineNumber == lineNumber).isCompleted = false;
            }
            // when a block somewhere in the middle of a line is remmoved, current seq is decreased to blockSeq number - 1.
            else if (blockSequence < line.currentSequence)
            {
                lines.First(x => x.lineNumber == lineNumber).currentSequence = blockSequence - 1;
                lines.First(x => x.lineNumber == lineNumber).isCompleted = false;
            }
        }
    }
}

