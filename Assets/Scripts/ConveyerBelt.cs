using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyerBelt : MonoBehaviour
{
    private GameObject grid;
    private GameObject bunny;
    private GameObject cat;
    private GameObject bear;
    private ThreeDGrid threeDGrid;

    [SerializeField] private CutsceneManager cutsceneManager;

    void Start()
    {
        grid = GameObject.Find("Grid");
        threeDGrid = grid.GetComponent<ThreeDGrid>();

        bunny = GameObject.Find("Bunny");
        cat = GameObject.Find("Kitty");
        bear = GameObject.Find("Teddy");

        bunny.SetActive(false);
        cat.SetActive(false);
        bear.SetActive(false);
    }

    void Update()
    {
        GridTile block = threeDGrid.GetGrid()[7, 4];
        if (block.type == TileType.PUSHABLE)
        {
            GameObject obj = null; 
            PushableObject pushableObject = block.gameObj.GetComponent<PushableObject>();
            switch (pushableObject.codeString)
            {
                case "bunny":
                    bunny.SetActive(true);
                    break;
                case "bear":
                    bear.SetActive(true);
                    break;
                case "cat":
                    cat.SetActive(true);
                    break;
            }

            cutsceneManager.PlayCutscene();
        }
        else
        {
            bunny.SetActive(false);
            bear.SetActive(false);
            cat.SetActive(false);

            cutsceneManager.isPlaying = true;
        }
    }
}
