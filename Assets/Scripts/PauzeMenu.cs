using UnityEngine;
using UnityEngine.SceneManagement;

public class PauzeMenu : MonoBehaviour
{
    public bool pauzed = false;
    public GameObject pauzeMenuUI;
    public GameObject optionsMenuUI;

    private void Update()
    {
        //When escape is pressed you see the pauze menu 
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pauseButton();
        }
    }
    public void pauseButton()
    {
        //When the pause button is pressed you see the pauze menu and the game will pauze
        if (pauzed == false)
        {
            pauzeMenuUI.SetActive(true);
            Time.timeScale = 0f;
            pauzed = true;
        }
        else
        {
            ResumeButton();
        }
    }
    public void ResumeButton()
    {
        //When the resume button is pressed the game continues
        optionsMenuUI.SetActive(false);
        pauzeMenuUI.SetActive(false);
        Time.timeScale = 1f;
        pauzed = false;
    }
    public void MenuButton()
    {
        //When the menu button is pressed you go to the main menu
        SceneManager.LoadScene(sceneName: "MainMenu");
        pauzed = false;
        optionsMenuUI.SetActive(false);
        pauzeMenuUI.SetActive(false);
        Time.timeScale = 1f;

        GameObject.FindGameObjectWithTag("Music").GetComponent<MusicManager>().StopMusic();
    }
    public void ResetLevelActivate()
    {
        //When the reset button is pressed the scene will reset
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        ResumeButton();
    }
    public void ExitButton()
    {
        //When the exit button is pressed you exit the game
        Application.Quit();
    }
}
