using UnityEngine;
using FMODUnity;

public class MusicManager : MonoBehaviour
{
    private float volume = 0.0f;

    public FMODUnity.EventReference musicEvent;
    public static FMOD.Studio.EventInstance introMusic;

    private static MusicManager instance;

    private void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            introMusic = FMODUnity.RuntimeManager.CreateInstance(musicEvent);
        }
    }

    public void PlayMusic(string name, float value)
    {
        if (!introMusic.isValid())
        {
            introMusic = FMODUnity.RuntimeManager.CreateInstance(musicEvent);
        }

        introMusic.setParameterByName(name, value);

        if (IsPlaying(introMusic) == FMOD.Studio.PLAYBACK_STATE.PLAYING) return;
        introMusic.start();
    }

    public void StopMusic()
    {
        introMusic.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }

    FMOD.Studio.PLAYBACK_STATE IsPlaying(FMOD.Studio.EventInstance instance)
    {
        FMOD.Studio.PLAYBACK_STATE state;
        instance.getPlaybackState(out state);
        return state;
    }
}