using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Writer : MonoBehaviour
{
    public void Run(string text, TMP_Text textField, Button next = null)
    {
        StartCoroutine(Write(text, textField, next));
    }

    public IEnumerator Write(string text, TMP_Text textField, Button next = null)
    {
        float time = 0;
        int charIndex = 0;

        while (charIndex < text.Length)
        {
            time += Time.deltaTime * 50f;
            charIndex = Mathf.FloorToInt(time);
            charIndex = Mathf.Clamp(charIndex, 0, text.Length);

            textField.text = text.Substring(0, charIndex);
            yield return null;
        }

        textField.text = text;
        if(next != null)
            next.gameObject.SetActive(true);
    }
}
