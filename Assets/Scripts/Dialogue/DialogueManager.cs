using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    [SerializeField] private TMP_Text textField;
    [SerializeField] private GameObject dialogue;
    [SerializeField] private Image speakerImage;
    [SerializeField] private Button nextButton;
    [SerializeField] string text1;
    [SerializeField] string text2;
    [SerializeField] string text3;
    private int textAmount = 0;
    private GameObject canvas;
    private DialogueManager dialogueManager;
    private bool dialogueStarted = false;
    private int textCount = -1;
    private List<TextToWrite> multipleTexts = new List<TextToWrite>();
    private void Start()
    {
        dialogue.SetActive(false);
        nextButton.gameObject.SetActive(false);

        canvas = this.gameObject;
    }

    private void Update()
    {
        if (!dialogueStarted)
        {
            List<TextToWrite> test = new List<TextToWrite>();
            dialogueManager = canvas.GetComponent<DialogueManager>();
            if (text1.Length != 0)
            {
                TextToWrite text = new TextToWrite() { text = text1, speaker = "alien" };
                test.Add(text);
                if (text2.Length != 0)
                {
                    text = new TextToWrite() { text = text2, speaker = "alien" };
                    test.Add(text);
                    if (text3.Length != 0)
                    {
                        text = new TextToWrite() { text = text3, speaker = "alien" };
                        test.Add(text);
                    }
                }
                dialogueManager.WriteMultipleTexts(test);
                dialogueStarted = true;
            }
           
            
        }
    }
    /// <summary>
    /// Call to write text
    /// </summary>
    /// <param name="text"></param>
    /// <param name="next"></param>
    public void WriteText(TextToWrite text, Button next = null)
    {
        Texture2D myTexture = Resources.Load(text.speaker) as Texture2D;
        Rect rec = new Rect(0, 0, myTexture.width, myTexture.height);

        speakerImage.sprite = Sprite.Create(myTexture, rec, new Vector2(0, 0), 1);

        dialogue.SetActive(true);

        canvas.GetComponent<Writer>().Run(text.text, textField, next);
    }
    /// <summary>
    /// Call to write multiple texts, Next button will be activated untill last text.
    /// </summary>
    /// <param name="texts"></param>
    public void WriteMultipleTexts(List<TextToWrite> texts = null)
    {
        if (multipleTexts.Count == 0 && texts.Count > 0)
            multipleTexts = texts;

        textCount++;
        if (textCount == multipleTexts.Count - 1 && multipleTexts.Count != 0)
        {
            nextButton.gameObject.SetActive(false);
            WriteText(multipleTexts[textCount]);
            multipleTexts.Clear();
            textCount = -1;
        }
        else
            WriteText(multipleTexts[textCount], nextButton);
    }
    /// <summary>
    /// Write next text
    /// </summary>
    public void HandleNextButton()
    {
        nextButton.gameObject.SetActive(false);
        WriteMultipleTexts();
    }
    /// <summary>
    /// Close the dialoge
    /// </summary>
    public void CloseDialogue()
    {
        multipleTexts.Clear();
        textCount = -1;
        dialogue.SetActive(false);
    }
}
