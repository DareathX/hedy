using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextToWrite
{
    public string text { get; set; }
    public string speaker { get; set; }
}
