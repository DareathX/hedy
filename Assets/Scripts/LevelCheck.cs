using UnityEngine;

public class LevelCheck : MonoBehaviour
{
    public int level = 1;
    private GameObject[] npcs;

    private static LevelCheck Instance;
    void Awake()
    {
        DontDestroyOnLoad(this);

        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Update()
    {
        npcs = GameObject.FindGameObjectsWithTag("NPC"); 
        if (npcs.Length > 0)
            for (int i = 0; i < npcs.Length; i++)
            {
                if (npcs[i] != null){
                    if (npcs[i].GetComponent<NpcController>().level <= level) // All npcs before and of current level need to be active. 
                    {
                        npcs[i].SetActive(true);
                    }
                    else npcs[i].SetActive(false);  // npcs greater than current level need to be inactive
                }
            }
    }
}
