using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMovement : MonoBehaviour
{
    public Texture unpressed, pressed;

    [SerializeField]
    private GameObject up, right, down, left;

    private PauzeMenu pause;

    private void Awake()
    {
        // DO NOT change the name of MenuCanvas in the scenes
        pause = GameObject.Find("MenuCanvas").GetComponent<PauzeMenu>();
    }

    private void Update()
    {
        if (!pause.pauzed)
        {
            ChangeTexture();
        }
    }

    public void ChangeTexture()
    {
        if (Input.GetKey("up") || Input.GetKey(KeyCode.W))
        {
            up.GetComponent<RawImage>().texture = pressed;
        }
        else
        {
            up.GetComponent<RawImage>().texture = unpressed;
        }

        if (Input.GetKey("right") || Input.GetKey(KeyCode.D))
        {
            right.GetComponent<RawImage>().texture = pressed;
        }
        else
        {
            right.GetComponent<RawImage>().texture = unpressed;
        }

        if (Input.GetKey("down") || Input.GetKey(KeyCode.S))
        {
            down.GetComponent<RawImage>().texture = pressed;
        }
        else
        {
            down.GetComponent<RawImage>().texture = unpressed;
        }

        if (Input.GetKey("left") || Input.GetKey(KeyCode.A))
        {
            left.GetComponent<RawImage>().texture = pressed;
        }
        else
        {
            left.GetComponent<RawImage>().texture = unpressed;
        }
    }
}
