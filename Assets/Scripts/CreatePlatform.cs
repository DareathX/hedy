using UnityEngine;

public class CreatePlatform : MonoBehaviour
{
    public Vector3 gridOrigin = Vector3.zero;
    public GameObject[] tiles;
    public int gridX, gridZ;
    public float gridSpacingOfsset = 10f;

    private void Start()
    {
        SpawnGrid();

    }
    private void SpawnGrid()
    {
        for (int x = 0; x < gridX; x++)
        {
            for (int z = 0; z < gridZ; z++)
            {
                Vector3 spawnPosition = new Vector3(x * gridSpacingOfsset, 0, z * gridSpacingOfsset) + gridOrigin;
                PickAndSpawn(spawnPosition, Quaternion.Euler(new Vector3(0, -180, 0)));
            }
        }
    }

    private void PickAndSpawn(Vector3 positionToSpawn, Quaternion rotationToSpawn)
    {
        GameObject clone = Instantiate(tiles[0], positionToSpawn, rotationToSpawn);
        clone.name = "Tiles";
        clone.transform.SetParent(this.transform);
    }
}