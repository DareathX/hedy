public enum TileType
{
    EMPTY,
    WALL,
    OBSTACLE,
    PUSHABLE,
    PLAYER
}
