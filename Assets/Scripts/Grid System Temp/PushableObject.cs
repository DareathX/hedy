using Assets.Scripts.BlockSequence;
using System.Collections.Generic;
using UnityEngine;

public class PushableObject : MonoBehaviour
{
    private GameObject gridObj;
    private Stack<Vector3> moves;
    private Vector3 newPos;
    private Vector3 lastPos;
    private bool isMoving;
    private float duration;
    private float elapsedTime;

    private ThreeDGrid grid;
    private BlockSequenceManager blockSequenceManager;

    public int codeLineNumber;
    public int blockSequence;
    public string codeString;
    public CodeType codeType;

    private void Start()
    {
        gridObj = GameObject.Find("Grid");
        grid = gridObj.GetComponent<ThreeDGrid>();
        blockSequenceManager = gridObj.GetComponent<BlockSequenceManager>();
        if (blockSequenceManager.lines.Count == 0)
            blockSequenceManager.lines = gridObj.GetComponent<ICodeLineFactory>().GetSceneCodeLines();
        moves = new Stack<Vector3>();
        newPos = transform.localPosition;
        moves.Push(newPos);
        isMoving = false;
        duration = 0.5f;
    }

    private void Update()
    {
        elapsedTime += Time.deltaTime;
    }

    private void FixedUpdate()
    {
        MoveToNewPosition();
    }

    private void MoveToNewPosition()
    {
        if (!transform.localPosition.Equals(newPos))
        {
            // Moving to the new position untill it is at the new position
            float percentage = elapsedTime / duration;
            transform.localPosition = Vector3.Lerp(lastPos, newPos, Mathf.SmoothStep(0, 1, percentage));
            CheckNearBlocks();
        }
        else
        {
            isMoving = false;
        }
    }

    public bool SetNewPos(Vector3 pos)
    {
        // Check if the position is empty where the player wants to push an object. If yes then it gets pushed
        if (!grid.CanPushHere(pos)) return false;
        grid.MoveHere(transform.localPosition, pos);
        isMoving = true;
        lastPos = newPos;
        newPos = pos;
        moves.Push(pos);
        elapsedTime = 0;
        return true;
    }

    public bool UndoBlock()
    {
        Vector3 tempPos = moves.Pop();
        print((moves.Count < 1) + " " + isMoving + " " + !grid.CanPushHere(moves.Peek()));
        if (moves.Count < 1 || isMoving || !grid.CanPushHere(moves.Peek()))
        {
            moves.Push(tempPos);
            return false;
        }
        elapsedTime = 0;
        isMoving = true;
        lastPos = tempPos;
        newPos = moves.Peek();
        grid.MoveHere(transform.localPosition, newPos);
        return true;
    }

    public bool IsMoving() { return isMoving; }

    /// <summary>
    /// Checks if there is a pushable block on the left or on "top" of this pushable block. 
    /// If the block on the left or on top belongs to the same line of code and the sequence matches,
    /// the new sequence gets added to the LineOfCode
    /// </summary>
    public void CheckNearBlocks()
    {
        Vector3 currentPosition = transform.localPosition;

        GridTile blockOnTheLeft = grid.GetGrid()[(int)currentPosition.x - 1, (int)currentPosition.z];

        if (blockOnTheLeft.type == TileType.PUSHABLE)
        {
            PushableObject pushableObject = blockOnTheLeft.gameObj.GetComponent<PushableObject>();
            if (pushableObject.codeLineNumber == codeLineNumber && pushableObject.blockSequence == blockSequence - 1)
            {
                blockSequenceManager.AddSequenceToLine(codeLineNumber, blockSequence);
            }
        }
        else
        {
            blockSequenceManager.RemoveSequenceFromLine(codeLineNumber, blockSequence);
        }
    }
}
