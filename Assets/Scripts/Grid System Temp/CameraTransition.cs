using UnityEngine;
using Cinemachine;

public class CameraTransition : MonoBehaviour
{
    private CinemachineVirtualCamera grid;
    private CinemachineVirtualCamera free;
    private CinemachineVirtualCamera topDown;

    private void Start()
    {
        grid = GameObject.Find("Camera_Grid").GetComponent<CinemachineVirtualCamera>();
        free = GameObject.Find("Camera_Free").GetComponent<CinemachineVirtualCamera>();
        topDown = GameObject.Find("Camera_TopDown").GetComponent<CinemachineVirtualCamera>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            SwitchCamera("topDown");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            SwitchCamera("grid");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            SwitchCamera("free");
        }
    }

    private void SwitchCamera(string cam)
    {
        switch (cam)
        {
            case "topDown":
                topDown.Priority = 21 - topDown.Priority;
                break;
            case "grid":
                grid.Priority = 20;
                free.Priority = 10;
                break;
            case "free":
                grid.Priority = 10;
                free.Priority = 20;
                break;
            default:
                break;
        }
    }
}
