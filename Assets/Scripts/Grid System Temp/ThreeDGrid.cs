using UnityEngine;

public class ThreeDGrid : MonoBehaviour
{
    [SerializeField]
    private GameObject wall;
    [SerializeField]
    private GameObject cellPref;
    private GameObject gridObj;
    private GameObject cellObj;
    private int width;
    private int depth;
    private GridTile[,] gridList;

    private void Start()
    {
        gridObj = transform.gameObject;
        width = (int)gridObj.transform.lossyScale.x;
        depth = (int)gridObj.transform.lossyScale.z;
        gridList = new GridTile[width + 1, depth + 1];
        cellObj = new GameObject("Cells");
        cellObj.transform.SetParent(gameObject.transform, true);
        cellObj.transform.localPosition = new Vector3(cellObj.transform.position.x, transform.localScale.y / 2, cellObj.transform.position.z);
        InitGrid();
    }

    // Creates the grid based on te size of the Grid object
    private void InitGrid()
    {
        Transform[] objects = transform.parent.GetComponentsInChildren<Transform>();
        for (int x = 0; x <= width; x++)
        {
            for (int z = 0; z <= depth; z++)
            {
                if (x == 0 || x == width || z == 0 || z == depth)
                {
                    Instantiate(wall, new Vector3(x + transform.parent.position.x, gridObj.transform.position.y, z + transform.parent.position.z), transform.rotation, transform.parent);
                    gridList[x, z] = new GridTile(TileType.WALL);
                }
                else
                {
                    gridList[x, z] = new GridTile(TileType.EMPTY);
                    GameObject temp = Instantiate(cellPref, new Vector3(x + transform.parent.position.x, 0, z + transform.parent.position.z), transform.rotation, cellObj.transform);
                    temp.transform.localPosition = new Vector3(temp.transform.localPosition.x, 0, temp.transform.localPosition.z);
                }
            }
        }
        foreach (Transform tile in objects)
        {
            if (tile.name == "Grid" || tile.name == "GameMap") continue;
            gridList[(int)tile.localPosition.x, (int)tile.localPosition.z].gameObj = tile.gameObject;
            switch (tile.tag)
            {
                case "GridWall":
                    gridList[(int)tile.localPosition.x, (int)tile.localPosition.z].type = TileType.WALL;
                    break;
                case "Obstacle":
                    gridList[(int)tile.localPosition.x, (int)tile.localPosition.z].type = TileType.OBSTACLE;
                    break;
                case "Pushable":
                    gridList[(int)tile.localPosition.x, (int)tile.localPosition.z].type = TileType.PUSHABLE;
                    break;
                default:
                    gridList[(int)tile.localPosition.x, (int)tile.localPosition.z].type = TileType.EMPTY;
                    break;
            }
        }
    }

    public bool CanPushHere(Vector3 next)
    {
        if (gridList.GetLength(0) <= (int)next.x || gridList.GetLength(1) <= (int)next.z) return false;
        if (gridList[(int)next.x, (int)next.z].type != TileType.EMPTY) return false;
        return true;
    }

    public GameObject PushableHere(Vector3 targetBlock)
    {
        if (gridList[(int)targetBlock.x, (int)targetBlock.z].type != TileType.PUSHABLE) return null;
        return gridList[(int)targetBlock.x, (int)targetBlock.z].gameObj;
    }

    public void MoveHere(Vector3 cur, Vector3 next)
    {
        // Switch the values of the current and the next tile to the object that gets pushed
        GridTile temp = gridList[(int)cur.x, (int)cur.z];
        gridList[(int)cur.x, (int)cur.z] = gridList[(int)next.x, (int)next.z];
        gridList[(int)next.x, (int)next.z] = temp;
    }

    public void PlacePlayer(Vector3 pos)
    {
        gridList[(int)pos.x, (int)pos.z].gameObj = GameObject.FindGameObjectWithTag("Player");
        gridList[(int)pos.x, (int)pos.z].type = TileType.PLAYER;
    }

    public void RemovePlayer()
    {
        foreach (GridTile tile in gridList)
        {
            if (tile.type == TileType.PLAYER)
            {
                tile.gameObj = null;
                tile.type = TileType.EMPTY;
            }
        }
    }

    public GridTile[,] GetGrid()
    {
        return gridList;
    }
}