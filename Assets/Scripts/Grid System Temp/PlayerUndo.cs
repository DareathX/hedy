using System.Collections.Generic;
using UnityEngine;

public class PlayerUndo : MonoBehaviour
{
    private Stack<PushableObject> pushableObjects;
    private void Start()
    {
        pushableObjects = new Stack<PushableObject>();
    }

    private void Update()
    {
        CheckIfUndo();
    }

    private void CheckIfUndo()
    {
        if (Input.GetKeyDown(KeyCode.Z) && pushableObjects.Count > 0)
        {
            PushableObject pushableObject = pushableObjects.Pop();
            bool undo = false;
            undo = pushableObject.UndoBlock();
            if (!undo) pushableObjects.Push(pushableObject);
        }
    }

    public void PushPushableObject(PushableObject pushableObject)
    {
        pushableObjects.Push(pushableObject);
    }
}
