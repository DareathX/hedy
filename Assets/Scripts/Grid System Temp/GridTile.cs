using UnityEngine;

public class GridTile
{
    public TileType type;
    public GameObject gameObj;

    public GridTile(TileType type, GameObject gameObj = null)
    {
        this.type = type;
        this.gameObj = gameObj;
    }
}
