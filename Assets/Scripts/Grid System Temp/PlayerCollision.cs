using System.Collections;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    private PlayerUndo playerUndo;
    private Movement movement;
    private ThreeDGrid grid;
    private bool isPushing;
    private void Start()
    {
        playerUndo = transform.GetComponent<PlayerUndo>();
        movement = transform.GetComponent<Movement>();
        isPushing = false;

        GetGrid();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Space) && movement.IsInGrid())
        {
            GameObject obj = grid.PushableHere(movement.GetNextGridPosition() - grid.transform.parent.transform.position);
            if (obj != null && !isPushing)
            {
                isPushing = true;
                movement.Push();
                StartCoroutine(Pushing(obj));
                StartCoroutine(TurnOnPushing());
            }
        }
    }

    private IEnumerator Pushing(GameObject obj)
    {
        yield return new WaitForSeconds(.2f);
        PushObject(obj);
    }

    private IEnumerator TurnOnPushing()
    {
        yield return new WaitForSeconds(.5f);
        isPushing = false;
    }

    private void PushObject(GameObject obj)
    {
        PushableObject pushableObj = obj.GetComponent<PushableObject>();
        // Statement blocks can not be pushed.
        if (pushableObj.IsMoving() || pushableObj.codeType == CodeType.Statement) return;
        // Get the direction of which side the player is pushing the object from
        Vector3 dir = obj.transform.position - transform.position;
        dir.y = 0;
        if (Mathf.Abs(dir.x) > Mathf.Abs(dir.z)) dir.z = 0;
        else dir.x = 0;
        dir.Normalize();
        bool moved = pushableObj.SetNewPos(obj.transform.localPosition + dir * 1f);
        if (!moved) return;
        playerUndo.PushPushableObject(pushableObj);
    }

    public bool IsPushing()
    {
        return isPushing;
    }

    private void GetGrid()
    {
        GameObject go = GameObject.Find("Grid");
        if (go != null)
        {
            grid = go.GetComponent<ThreeDGrid>();
        }
    }
}
