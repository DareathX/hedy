using UnityEngine;

public class Bouncing : MonoBehaviour
{
    public float speed = 0.2f;
    private float ogY;

    public void Start()
    {
        Vector3 pos = transform.TransformDirection(gameObject.transform.position);
        ogY = pos.y + 1;
    }
    public void Update()
    {
        float y = Mathf.PingPong(Time.time * speed, 0.1f) * 1.5f - 3;
        y += ogY;
        gameObject.transform.position = new Vector3(gameObject.transform.position.x, y, gameObject.transform.position.z);
    }
}